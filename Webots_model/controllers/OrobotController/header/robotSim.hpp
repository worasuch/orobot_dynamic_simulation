#ifndef ROBOTSIM_HPP
#define ROBOTSIM_HPP


#define OPTIMIZATION
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include <sys/time.h>
#include "joystick.h"
#include "utils.hpp"
#include <fstream>
#include <iostream>
#include <webots/Supervisor.hpp>
#include <webots/Motor.hpp>
#include <webots/PositionSensor.hpp>
#include <webots/Robot.hpp>
#include <webots/GPS.hpp>
#include <webots/Gyro.hpp>
#include <webots/Accelerometer.hpp>
#include <webots/Receiver.hpp>
#include <webots/Compass.hpp>
#include <webots/TouchSensor.hpp>
#include <webots/Node.hpp>
#include <webots/Field.hpp>
#include "misc_math.hpp"




using namespace std;
using namespace Eigen;
//using namespace Robot;
//using Eigen::Matrix Matrix;



class RobotSim : public webots::Supervisor{


  public:
    //================== public variables ===============================================
    vector<webots::Motor*> rm_motor;
    vector<webots::PositionSensor*> ps_motor;
    webots::GPS *gps;
    webots::Receiver *rec;

    webots::Node *fgirdle, *FL_marker, *FR_marker, *HL_marker, *HR_marker, *roboDef, *viewcam;
    const double *gpsData, *ts_fl, *ts_fr, *ts_hl, *ts_hr, *rotMat, *posFL, *posFR, *posHL, *posHR;
    webots::Field *roboRot, *roboPos;


    //================== public functions ===============================================
    RobotSim(int TIME_STEP); // constructor
    void setAngles(double*,int*);
    void ReadSensors(double *d_posture, double *d_torques, double *gyroData, double *accData, double *gpsData);
    void GetGPS(double *gpsData);
    void InitIMU();
    void getGRF(double *GRF);
    void ReadIMUAttitude(double *rotmat);
    void killSimulation();
    void setPositionRotation(double *p, double *r);
    void GetFeetGPS(double *FL_feet_gpos, double *FR_feet_gpos, double *HL_feet_gpos, double *HR_feet_gpos);
    void setServoMaxForce(double *force);
    void GetTailForce(double *tailForce);
    void getTorqueFeedback(double *d_torques);
    void getPositionFeedback(double *d_posture);
    void getAngles(double *table);
    void set2segFeetParam(double spring1, double spring2);
    void startVideoRecording(const string& filename, int width, int height);
    void stopVideoRecording();
    void modifyViewpoint();
  private:
    //================== private variables ===============================================




};


#endif

