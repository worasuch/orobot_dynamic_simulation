close
% front_humerus=0.143;
kk=0;

krange = {2351:2550; 2251:2450}
t = linspace(0,1,numel(krange{1}));

% titlestr={'Front limb shoulder torque', 'Hind limb hip torque'};
titlestr={'Front limb elbow torque', 'Hind limb knee torque'};
for k=[1 3]
    kk=kk+1;
    leg=k


    k_ind=230;
%     toggle between shoulde/hip and elbow/knee torques
%     jty=sqrt(sum( (joint_tor{k_ind}(:,[ind_yaw(leg) ind_pitch(leg) ind_roll(leg)])).^2, 2));   % shoulder / hip
    jty=sqrt(sum( (joint_tor{k_ind}(:,[ind_knee(leg)])).^2, 2));   % elbow/knee
    jty=smooth(jty,7);

    stance{k_ind}

    subplot(2,1,kk)
    plot(t, jty(krange{kk})), hold on
    ax=axis;
%     area(stance{k_ind}(:,leg), 'facealpha', 0.2)
    area(t, -stance{k_ind}(krange{kk},leg)*1000+1000, 'facealpha', 0.2); hold on
    axis(ax);
    title(titlestr{kk});
    ylabel('torque [N]');
    grid on;
end

