function plotOptilog(crit, critStr, param, settings, prc)

    colormap jet
    
    minprc=prctile(crit, prc);
    maxprc=prctile(crit, 100-prc);
    crit(crit<minprc)=minprc;
    crit(crit>maxprc)=maxprc;
    
    
    mincrit=min(crit);
    maxcrit=max(crit);
    sz=(crit-mincrit)/(maxcrit-mincrit)*200+5;

    scat=scatter3(param.ax1, param.ax2, param.ax3, sz, crit, 'filled'); hold on;


    xlabel('Long axis rotation [deg]'); 
    ylabel('Spine amplitude [deg]');
    zlabel('Abduction [deg]');

    scat.MarkerEdgeColor = [0 0 0];
    scat.Marker='s';

    view(-52,22);
    c=colorbar;
    grid on
    title({critStr ; ['Walking frequency: ' num2str(settings{1}(2)) 'Hz, Duty factor: ' num2str(settings{1}(3))]})
    box on
    ax = gca;
    ax.BoxStyle = 'full';
    ax.LineWidth = 1.5;
    ax.XDir='reverse';
    ax.YDir='reverse';




end

