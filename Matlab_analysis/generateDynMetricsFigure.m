clc
clear
close

% =======================LOAD MAT STRUCTURES============================
load RS_05_512
% figure(2)
% crit = {energyTV, energyTVAbs, energyTVZeroIg, energyTS, footTrajectoryReachability, stability, stabilityRate, absolutePrecision, maxTorquePerc', smoothness', stanceTorque};
% critStr = {'energyTV', 'energyTVAbs', 'energyTVZeroIg', 'energyTS', ...
%     'footTrajectoryReachability', 'stability', 'stabilityRate', 'absolutePrecision', 'maxTorquePerc', 'smoothness', 'stanceTorque'};

% =================READ FILES USED BY THE WEBSITE========================== 
% filepath = '/home/thorvat/Dropbox/OrobotProject/JAVASCRIPT/data/'
% d05 = importdata([filepath 'dataMatFINAL_joined_05Hz.csv']);
% d075 =importdata([filepath 'dataMatFINAL_joined_075Hz.csv']);
% dkin =importdata([filepath 'dataMat_kin.csv']);
% dcaiman = importdata('Caiman_hindlimb_validation.xlsx');



PARMAT = cell2mat(parameters)';
powerC = crit{3};      %energyTVZeroIg
tiltC = crit{7};       %stabilityRate
absprecC = crit{8};    %absolutePrecision
grfC = crit{12};    %absolutePrecision
%% INDIVIDUAL DYNAMIC
% % % set criteria and axis parameters
% crit = absprecC;
% param=paramH;
% 
% % normalize criteria with percentile
% minp=prctile(crit, 3);
% maxp=prctile(crit, 97);
% % minp=prctile(crit, 0);
% % maxp=prctile(crit, 100);
% 
% critS = crit;
% critS(critS>maxp) = maxp;
% critS(critS<minp) = minp;
% 
% critS = (critS - minp) / (maxp-minp);

%% KINEMATIC
% crit = dkin.data(:,end);   %boneCollBoth
% crit = dkin.data(:,end-2);   %boneCollF
% crit = dkin.data(:,end-1);   %boneCollH
% param = paramKinH;
% critS = crit;
%% COMBINED
NPC=50;

powerC(grfC==-1)=0;
tiltC(grfC==-1)=0;
absprecC(grfC==-1)=0;

powLim=prctile(powerC, NPC);
tiltLim=prctile(tiltC, NPC);
precLim=prctile(absprecC, NPC);
grfLim=prctile(grfC, NPC);




combScore = double(powerC>powLim)  + double(tiltC>tiltLim)   + double(absprecC>precLim) + double(grfC>grfLim);
% combScore =  double(grfC>grfLim)
critS = combScore;
param = paramH;
% critS = powerC
param.ax3 = -PARMAT(:,2)'/IG;
%%

% define scaling of markers
mincrit=min(critS);
maxcrit=max(critS);
sz=(critS-mincrit)/(maxcrit-mincrit)*250+15;

% sz1=sz; sz2=sz;
% sz1(kinscore < max(kinscore))=0.0001;
% sz2(kinscore == max(kinscore))=0.0001;
%define custom colormap
cmap0 = [165,0,38; ...
        215,48,39; ...
        244,109,67; ...
        253,174,97; ...
        254,224,144; ...
        224,243,248; ...
        171,217,233; ...
        116,173,209; ...
        69,117,180; ...
        49,54,149;]/255;
for k=1:3
    cmap(:,k) = interp1(1:numel(cmap0(:,1)), cmap0(:,k), 1:0.01:numel(cmap0(:,1)));
end
colormap(cmap);

% scatter plot
scat=scatter3(param.ax1, param.ax2, param.ax3, sz, critS, 'filled'); hold on; 
% scat=scatter3(param.ax1, param.ax2, param.ax3, sz, critS, 'filled'); hold on;

% scatter for combined
% scat=scatter3(param.ax1, param.ax2, param.ax3, sz1, critS, 'filled','MarkerFaceAlpha',.8); hold on;
% scat2=scatter3(param.ax1, param.ax2, param.ax3, sz2, critS, 'filled','MarkerFaceAlpha',.1); hold on;

% graph options
xlabel('Long axis rotation [deg]'); 
ylabel('Spine bending [deg]');
zlabel('Body Lift [deg]');

scat.MarkerEdgeColor = [0 0 0];
scat.Marker='o';

view(-52,22);
c=colorbar;
colorbar off;
grid on
% title({critStr ; ['Walking frequency: ' num2str(settings{1}(2)) 'Hz, Duty factor: ' num2str(settings{1}(3))]})
box on
ax = gca;
ax.BoxStyle = 'full';
ax.LineWidth = 1.5;
ax.XDir='reverse';
ax.YDir='reverse';



%% generate tikz
% cleanfigure('minimumPointsDistance',0.01)
% Power
% Tilting
% AbsPrec
% Kinematic
% % Combined
% matlab2tikz('Colorbar.tikz', 'height', '\figureheight', 'width', '\figurewidth');
% matlab2tikz('/data/Dropbox/OrobotProject/Figures/Colorbar.tikz', 'height', '\figureheight', 'width', '\figurewidth');

% matlab2tikz('/data/Dropbox/PHD THESIS/source/images/ch_Orobot/KinematicH.tikz', 'height', '\figureheight', 'width', '\figurewidth');





%% get point
% find([paramF.ax1; paramF.ax2; paramF.ax3]' == pp.Position)

[tf, index]=ismember([param.ax1; param.ax2; param.ax3]',pp.Position,'rows');
optind=find(index==1)
parameters{optind}




