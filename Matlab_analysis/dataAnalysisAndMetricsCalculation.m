%% ====================================   LOAD DATA ====================================
load('modern_grf.mat');
D=dir([filepath set '*optilog']);
nservos=28;
nspine=8;

N=numel(D);
n_settings=3;
n_parameters=5;

settings=cell(1,N);
parameters=cell(1,N);

%% GENERAL PARAMETERS / CONSTANTS
% leg indices
ind_yaw=nspine+[0 4 8 12]+1;
ind_pitch=nspine+[0 4 8 12]+2;
ind_roll=nspine+[0 4 8 12]+3;
ind_knee=nspine+[0 4 8 12]+4;

ind_fl = nspine + [0 1 2 3] +1;
ind_fr = nspine + [0 1 2 3] +1+4;
ind_hl = nspine + [0 1 2 3] +1+8;
ind_hr = nspine + [0 1 2 3] +1+12;

%% generate trackways
or_Ltot=0.935*IG*igScl;
or_Width=0.53*IG*igScl;
or_deltaFH=0.3305*IG*igScl;

HL=[0:or_Ltot:10]';
HL(:,2)=or_Width/2;

FL=HL;  FL(:,1)=FL(:,1)+or_deltaFH;

HR=HL;  HR(:,1)=HR(:,1)-or_Ltot/2; HR(:,2)=-HR(:,2);

FR=HR;  FR(:,1)=FR(:,1)+or_deltaFH;

tracks=[FL, FR, HL, HR];


footstepsPoint1=cell(N,1);
footstepsPoint2=cell(N,1);
footstepsPoint3=cell(N,1);
footstepsPoint4=cell(N,1);

grf=cell(N,1);
stance=cell(N,1);
GRFm_int=cell(N,1);
GRFcorr=cell(N,1);
GRFabs=cell(N,1);
GRFsq=cell(N,1);
GRF=cell(N,1);
ST=cell(N,1);
GRF_MAT=cell(N,1);

GRF_MAT_ds=cell(N,1);
GRF_MAT_ds_hind=cell(N,1);
GRFcorr_ds=cell(N,1);
GRFabs_ds=cell(N,1);
GRFsq_ds=cell(N,1);
parfor k=KK  %230 the best
    kIndx{k} = k;
    disp(k)
    %========================================= DECYPHER NAME ==========================================
    name=D(k).name;
    sep=strfind(name,'_');
    par=strfind(name,'par');
    sep_set=sep(sep<par(1)); sep_par=sep(sep>par);

    for kk=1:numel(sep_set)-1
        settings{k}=[settings{k}; str2num(name(sep_set(kk)+1 : sep_set(kk+1)-1))];
    end
    for kk=1:numel(sep_par)-1
        parameters{k}=[parameters{k}; str2num(name(sep_par(kk)+1 : sep_par(kk+1)-1))];
    end

    
    %=========================================  READ FILES  ==========================================
    data=dlmread([filepath set name]); 
    t{k}=data(:, 1); data(:, 1)=[];
    joint_ang{k}=data(:, 1:nservos); data(:, 1:nservos)=[];
    joint_tor{k}=data(:, 1:nservos); data(:, 1:nservos)=[];
    forRPY{k}=data(:, 1:3); data(:, 1:3)=[];
    fgirdGPS{k}=data(:, 1:3); data(:, 1:3)=[];
    feetGPS{k}=data(:, 1:12); data(:, 1:12)=[];
    legPhase{k}=data(:, 1:4); data(:, 1:4)=[];
    trackingError{k}=data(:, 1:4); data(:, 1:4)=[];
    if size(data,2)>1 
        girdOrientationPresent{k}=1;
        girdOrientation=data(:,1:2); data(:,1:2)=[];
        girdAmpF{k} = prctile(abs(girdOrientation(:,1)), 99);
        girdAmpH{k} = prctile(abs(girdOrientation(:,2)), 99);
    else
        girdOrientationPresent{k}=0;
    end
    
    if size(data,2)==12 
        grfPresent{k}=1;
        grf{k}=data(:,1:12)/oroMass; data(:,1:12)=[];
    else
        grfPresent{k}=0;
    end
    
    
    
    
    %=========================================  CALC METRICS  ==========================================
    % get time values and interesting part of the run (ignore first few steps)
    Tstart=1/settings{k}(2)*3;  % get start time to look into data
    indx=find(t{k}>=Tstart);
    Trun=t{k}(end)-t{k}(indx(1));
    dt=t{k}(2)-t{k}(1);
    duty=settings{k}(3);
    
    % %%%%%%%%%%%%%%%%%%%%%%%% PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % detect leg stance phases
    stanceTol=0.0;   %FULL STANCE FOR PARAMETERS
    stancePhase{k}=legPhase{k}>=(0+stanceTol) & legPhase{k}<=(duty-stanceTol);
    
    stanceIndx1=find(legPhase{k}(:,1)>=(0+stanceTol) & legPhase{k}(:,1)<=(duty-stanceTol) & t{k}>Tstart);
    stanceIndx2=find(legPhase{k}(:,2)>=(0+stanceTol) & legPhase{k}(:,2)<=(duty-stanceTol) & t{k}>Tstart);
    stanceIndx3=find(legPhase{k}(:,3)>=(0+stanceTol) & legPhase{k}(:,3)<=(duty-stanceTol) & t{k}>Tstart);
    stanceIndx4=find(legPhase{k}(:,4)>=(0+stanceTol) & legPhase{k}(:,4)<=(duty-stanceTol) & t{k}>Tstart);
    minstancenum = min([numel(stanceIndx1) numel(stanceIndx2) numel(stanceIndx3) numel(stanceIndx4)]);
    stanceIndx1=stanceIndx1(1:minstancenum);
    stanceIndx2=stanceIndx2(1:minstancenum);
    stanceIndx3=stanceIndx3(1:minstancenum);
    stanceIndx4=stanceIndx4(1:minstancenum);
    
    
    % LAR and abduction (only LAR used)
    longAxisRotF(k)=max(joint_ang{k}(stanceIndx1, ind_roll(1))) - min(joint_ang{k}(stanceIndx1, ind_roll(1)));
    longAxisRotH(k)=max(joint_ang{k}(stanceIndx3, ind_roll(3))) - min(joint_ang{k}(stanceIndx3, ind_roll(3)));
    
    abductionF(k)=-mean(joint_ang{k}(stanceIndx1, ind_pitch(1)));
    abductionH(k)=-mean(joint_ang{k}(stanceIndx3, ind_pitch(3)));
    
    
    % %%%%%%%%%%%%%%%%%%%%%%%% METRICS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % detect leg stance phases - reduced for metrics calculation
    stanceTol=0.1125;   %15% of stance start and end removed
    stancePhase{k}=legPhase{k}>=(0+stanceTol) & legPhase{k}<=(duty-stanceTol);
    
    stanceIndx1=find(legPhase{k}(:,1)>=(0+stanceTol) & legPhase{k}(:,1)<=(duty-stanceTol) & t{k}>Tstart);
    stanceIndx2=find(legPhase{k}(:,2)>=(0+stanceTol) & legPhase{k}(:,2)<=(duty-stanceTol) & t{k}>Tstart);
    stanceIndx3=find(legPhase{k}(:,3)>=(0+stanceTol) & legPhase{k}(:,3)<=(duty-stanceTol) & t{k}>Tstart);
    stanceIndx4=find(legPhase{k}(:,4)>=(0+stanceTol) & legPhase{k}(:,4)<=(duty-stanceTol) & t{k}>Tstart);
    minstancenum = min([numel(stanceIndx1) numel(stanceIndx2) numel(stanceIndx3) numel(stanceIndx4)]);
    stanceIndx1=stanceIndx1(1:minstancenum);
    stanceIndx2=stanceIndx2(1:minstancenum);
    stanceIndx3=stanceIndx3(1:minstancenum);
    stanceIndx4=stanceIndx4(1:minstancenum);
    
    
    
    % measure velocity
    dist=norm(fgirdGPS{k}(end, :) - fgirdGPS{k}(indx(1), :));
    speed(k)=dist/Trun;
    
    %joint velocity
    joint_vel{k}=diff(joint_ang{k})/dt;
    joint_vel{k}=[joint_vel{k}; joint_vel{k}(end,:)];
    
    % measure energy
    torquesSquared(k)=sum(sum(joint_tor{k}(indx, 1:end).^2, 2)*dt)/Trun;

    temp_energy=(joint_tor{k}(indx, :)) .* (joint_vel{k}(indx, :));
    torquesVelocityAbs(k) = sum(sum(  abs(temp_energy)  ))/Trun;
    torquesVelocity(k) = sum(sum(  temp_energy  ))/Trun;
    
    temp_energy(temp_energy<0)=0;
    torquesVelocityZeroIg(k)= sum(sum(  temp_energy  ))/Trun;
    
    % torque percentile
    trqPercentile{k}=[];
    for kk=80:5:95
        trqPercentile{k}(end+1) = prctile(abs(joint_tor{k}(:)), kk);
    end
    
    % average torque during middle third of stance
    midStanceIndx1=find(legPhase{k}(:,1)>(duty/3) & legPhase{k}(:,1)<(2*duty/3) & t{k}>Tstart);
    midStanceIndx2=find(legPhase{k}(:,1)>(duty/3) & legPhase{k}(:,1)<(2*duty/3) & t{k}>Tstart);
    midStanceIndx3=find(legPhase{k}(:,1)>(duty/3) & legPhase{k}(:,1)<(2*duty/3) & t{k}>Tstart);
    midStanceIndx4=find(legPhase{k}(:,1)>(duty/3) & legPhase{k}(:,1)<(2*duty/3) & t{k}>Tstart);
    avgTorqueMidStance(k) =     sum(mean( abs( joint_tor{k}(midStanceIndx1, ind_fl) ),1)) + ...
                                sum(mean( abs( joint_tor{k}(midStanceIndx2, ind_fr) ),1)) + ...
                                sum(mean( abs( joint_tor{k}(midStanceIndx3, ind_hl) ),1)) + ...
                                sum(mean( abs( joint_tor{k}(midStanceIndx4, ind_hr) ),1));
    
    
    
    % measure trackingError
    trackingErrorStance(k)=sum(sum(trackingError{k}(stanceIndx1,1).*stancePhase{k}(stanceIndx1, 1)*dt))/Trun;    % FRONT LEFT
    
    % stability
    rollPitchNorm(k)=sum(sum(forRPY{k}(indx,1:2).^2))*dt/Trun;
    rollPitchNormRate(k)=sum(sum(diff(forRPY{k}(indx,1:2)/dt).^2))*dt/Trun;
    % fft analisys (smoothness of locomotion)
    Pfinal = 0;
    L_fft=size(fgirdGPS{k},1);
    for kk=1:3
        signal=fgirdGPS{k}(:,kk);
        Y=fft(signal);
        P2 = abs(Y/L_fft);
        P1 = P2(1:L_fft/2+1);
        P1(2:end-1) = 2*P1(2:end-1);
        Pfinal = Pfinal + P1;               % add fft signals of motion in 3 directions
    end
    f_fft = 1/dt*(0:(L_fft/2))/L_fft;
    highFreqComp{k}=[];
    for kk=1:1:5
        Flimit=kk;
        highFreqComp{k}(end+1) = sum(Pfinal(f_fft>Flimit));
    end
    
    

    
    
    % %%%%%%%%%%%%%%%%%%%%%%%% FOOTSTEPS %%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    [~, stepIndx1]=findpeaks(diff(stanceIndx1));
    [~, stepIndx2]=findpeaks(diff(stanceIndx2));
    [~, stepIndx3]=findpeaks(diff(stanceIndx3));
    [~, stepIndx4]=findpeaks(diff(stanceIndx4));

    footsteps{k}=[feetGPS{k}(stanceIndx1, [1 3]) feetGPS{k}(stanceIndx2, [4 6]) feetGPS{k}(stanceIndx3, [7 9]) feetGPS{k}(stanceIndx4, [10 12])];

    % mirror
    footsteps{k}(:, 2:2:end)=-footsteps{k}(:, 2:2:end);
    stanceIndx{k}=[stanceIndx1 stanceIndx2 stanceIndx3 stanceIndx4];
    
    % FOOTSTEPS METRICS WITH OPTIMIZATION
    [footsteps{k}, footstepsAdjustmentCost{k}, validatedFootsteps{k}, validatedTracks{k}]=footstepsRegression(footsteps{k}, tracks, stanceIndx{k});

    for kkk=1:numel(stepIndx1)-1
        footstepsPoint1{k}(kkk, 1:2)=mean( footsteps{k}((stepIndx1(kkk)+1:stepIndx1(kkk+1)), [1 2] ) );
    end
    for kkk=1:numel(stepIndx2)-1
        footstepsPoint2{k}(kkk, 1:2)=mean( footsteps{k}((stepIndx2(kkk)+1:stepIndx2(kkk+1)), [3 4]) );
    end
    for kkk=1:numel(stepIndx3)-1
        footstepsPoint3{k}(kkk, 1:2)=mean( footsteps{k}((stepIndx3(kkk)+1:stepIndx3(kkk+1)), [5 6]) );
    end
    for kkk=1:numel(stepIndx4)-1
        footstepsPoint4{k}(kkk, 1:2)=mean( footsteps{k}((stepIndx4(kkk)+1:stepIndx4(kkk+1)), [7 8]) );
    end
    
    
    
    %========================= GRF ===============================
    if grfPresent{k}
        stance{k}=legPhase{k}(:,:)<=duty;
        GRF{k}=zeros(floor(numel(indx)/(settings{k}(1)-3)),12);
        ST{k}=zeros(floor(numel(indx)/(settings{k}(1)-3)),4);

        % sum across multiple steps
        for kk=1:floor(numel(indx)/(settings{k}(1)-3)):numel(indx)-floor(numel(indx)/(settings{k}(1)-3))
            GRF{k} = GRF{k} + grf{k}(indx(kk:kk+floor(numel(indx)/(settings{k}(1)-3))-1  ), :);
            ST{k} = ST{k} + stance{k}(indx(kk:kk+floor(numel(indx)/(settings{k}(1)-3))-1  ), :);
        end
        GRF{k}=GRF{k}/(settings{k}(1)-3);
        ST{k}=ST{k}/(settings{k}(1)-3);


        % align them so stance is at the start
        for kk=1:4
            dst = diff(ST{k}(:,kk));
            if(min(dst)<-0.1)
                indxST = find(dst == min(dst));
                ST{k}(:,kk)=circshift(ST{k}(:,kk), -indxST); 
                GRF{k}(:,(kk-1)*3+1:kk*3)=circshift(GRF{k}(:,(kk-1)*3+1:kk*3), -indxST);  
                
                % remove swing phase
%                 tmp=GRF{k}(ST{k}(:,kk)>0,(kk-1)*3+1:kk*3);
%                 GRF{k}(:,(kk-1)*3+1:kk*3)=[];
%                 GRF{k}(:,(kk-1)*3+1:kk*3)=tmp;
%                 GRF{k}(ST{k}(:,kk), (kk-1)*3+1:kk*3)=[];
            end
        end
        % remove swing phase
        GRF{k}=GRF{k}(ST{k}(:,kk)>0,:);
        
        GRFm_int{k}=interp1(linspace(0, 1, numel(GRFm))',GRFm,linspace(0,1,size(GRF{k}, 1))');
        GRF_MAT{k}=[GRFm_int{k}, 0.5*GRF{k}(:,2)+0.5*GRF{k}(:,5),      smooth(0.5*GRF{k}(:,2)+0.5*GRF{k}(:,5), 10), smooth(0.5*GRF{k}(:,2)+0.5*GRF{k}(:,5), 20), ...
                          smooth(0.5*GRF{k}(:,2)+0.5*GRF{k}(:,5), 30), smooth(0.5*GRF{k}(:,2)+0.5*GRF{k}(:,5), 40), smooth(0.5*GRF{k}(:,2)+0.5*GRF{k}(:,5), 50)];
        tmp_GRFcorr = corrcoef(GRF_MAT{k});
        GRFcorr{k} = tmp_GRFcorr(1,:);
        
        
        GRFabs{k}(1,1)=0;
        GRFsq{k}(1,1)=0;
        for kk=2:size(GRF_MAT{k},2)
            GRFabs{k}(1,kk) = mean(abs(GRF_MAT{k}(:,1) - GRF_MAT{k}(:,kk)));
            GRFsq{k}(1,kk) = norm(GRF_MAT{k}(:,1) - GRF_MAT{k}(:,kk));
        end
        
        % downsampled
%         grf_ds=interp1(linspace(0,1,size(GRF{K(k)}, 1))',smooth(0.5*GRF{K(k)}(:,2)+0.5*GRF{K(k)}(:,5),7),linspace(0, 1, numel(GRFm))', 'linear');
        grf_ds_tmp=interp1(linspace(0,1,size(GRF{k}, 1))',smooth(0.5*GRF{k}(:,2)+0.5*GRF{k}(:,5),7),linspace(0, 1, numel(GRFm))');
        grf_ds_tmpH=interp1(linspace(0,1,size(GRF{k}, 1))',smooth(0.5*GRF{k}(:,8)+0.5*GRF{k}(:,11),7),linspace(0, 1, numel(GRFm))');
        GRF_MAT_ds{k}=[GRFm, grf_ds_tmp];
        GRF_MAT_ds_hind{k}=[GRFm, grf_ds_tmpH];
        tmp_GRFcorr = corrcoef(GRF_MAT_ds{k});
        GRFcorr_ds{k} = tmp_GRFcorr(1,:);
        
        GRFabs_ds{k}(1,1)=0;
        GRFsq_ds{k}(1,1)=0;
        for kk=2:size(GRF_MAT_ds{k},2)
            GRFabs_ds{k}(1,kk) = mean(abs(GRF_MAT_ds{k}(:,1) - GRF_MAT_ds{k}(:,kk)));
            GRFsq_ds{k}(1,kk) = norm(GRF_MAT_ds{k}(:,1) - GRF_MAT_ds{k}(:,kk));
        end
        
            
    end
    
    
    
    
    %========================= release memory ===============================
   
    t{k}=[];
    joint_ang{k}=[];
    joint_tor{k}=[];
    forRPY{k}=[];
    fgirdGPS{k}=[];
    feetGPS{k}=[];
    legPhase{k}=[];
    trackingError{k}=[];
    
    joint_vel{k}=[];
    footsteps{k}=[];
    stanceIndx{k}=[];
    
    
%     grf{k}=[];
%     stance{k}=[];
%     GRFm_int{k}=[];
%     GRFcorr{k}=[];
%     GRFabs{k}=[];
%     GRFsq{k}=[];
%     GRF{k}=[];
%     ST{k}=[];1
%     GRF_MAT{k}=[];
    
    

end



par_grid=cell2mat(parameters);
spineBendingParameters=par_grid(1,:);
bodyHeightParameters=par_grid(2,:);
yawVsRollParameters=par_grid(3,:);

if girdOrientationPresent{1} == 1
    girdAmpF=cell2mat(girdAmpF);
    girdAmpH=cell2mat(girdAmpH);
    
end

trqPercentile = cell2mat(trqPercentile');
highFreqComp = cell2mat(highFreqComp');
footstepsAdjustmentCost=cell2mat(footstepsAdjustmentCost);


% adjust GRF metrics
% GRFcorrM = cell2mat(GRFcorr);
% GRFabsM = cell2mat(GRFabs);
% GRFsqM = cell2mat(GRFsq);
% for k=2:size(GRFabsM,2)
%     GRFabsM(:,k) = -GRFabsM(:,k)-min(-GRFabsM(:,k));
%     GRFsqM(:,k) = -GRFsqM(:,k)-min(-GRFsqM(:,k));
% end

GRFcorrM_ds = cell2mat(GRFcorr_ds);
GRFabsM_ds = cell2mat(GRFabs_ds);
GRFsqM_ds = cell2mat(GRFsq_ds);


for k=2:size(GRFabsM_ds,2)
    GRFabsM_ds(:,k) = -GRFabsM_ds(:,k)-min(-GRFabsM_ds(:,k));
    GRFsqM_ds(:,k) = -GRFsqM_ds(:,k)-min(-GRFsqM_ds(:,k));
end

