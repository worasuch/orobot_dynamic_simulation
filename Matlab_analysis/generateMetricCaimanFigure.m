clc
clear
close
% =======================LOAD MAT STRUCTURES============================
% load gaitSearchFINAL05
% load KINDATA
% crit = {energyTV, energyTVAbs, energyTVZeroIg, energyTS, footTrajectoryReachability, stability, stabilityRate, absolutePrecision, maxTorquePerc', smoothness', stanceTorque};
% critStr = {'energyTV', 'energyTVAbs', 'energyTVZeroIg', 'energyTS', ...
%     'footTrajectoryReachability', 'stability', 'stabilityRate', 'absolutePrecision', 'maxTorquePerc', 'smoothness', 'stanceTorque'};

% =================READ FILES USED BY THE WEBSITE========================== 

dcaiman = importdata('./data/Caiman_hindlimb_validation.xlsx');



DATA = dcaiman;

paramKinH.ax1 = [DATA.data(:,3)];  %longAxisRotH
paramKinH.ax2 = [DATA.data(:,2)]/2;  %spineBendingParametersH
paramKinH.ax3 = [DATA.data(:,1)]; %bodyHeightIGD

kinscore = DATA.data(:, 4);    %scoreHind


%% KINEMATIC
param = paramKinH; 
critS = kinscore;

%%

% define scaling of markers
mincrit=min(critS);
maxcrit=max(critS);
sz=(critS-mincrit)/(maxcrit-mincrit)*250+15;

sz1=sz; sz2=sz;
sz1(kinscore < max(kinscore))=0.0001;
sz2(kinscore == max(kinscore))=0.0001;
%define custom colormap
cmap0 = [165,0,38; ...
        215,48,39; ...
        244,109,67; ...
        253,174,97; ...
        254,224,144; ...
        224,243,248; ...
        171,217,233; ...
        116,173,209; ...
        69,117,180; ...
        49,54,149;]/255;
for k=1:3
    cmap(:,k) = interp1(1:numel(cmap0(:,1)), cmap0(:,k), 1:0.01:numel(cmap0(:,1)));
end
colormap(cmap);

% scatter plot
scat=scatter3(param.ax1, param.ax2, param.ax3, sz, critS, 'filled','MarkerFaceAlpha',1); hold on;
% scat=scatter3(param.ax1, param.ax2, param.ax3, sz, critS, 'filled'); hold on;

% scatter for combined
% scat=scatter3(param.ax1, param.ax2, param.ax3, sz1, critS, 'filled','MarkerFaceAlpha',.8); hold on;
% scat2=scatter3(param.ax1, param.ax2, param.ax3, sz2, critS, 'filled','MarkerFaceAlpha',.1); hold on;

% graph options
xlabel('Long axis rotation [deg]'); 
ylabel('Spine bending [deg]');
zlabel('Body Lift [deg]');

scat.MarkerEdgeColor = [0 0 0];
scat.Marker='o';

view(-52,22);
c=colorbar;
colorbar off;
grid on
% title({critStr ; ['Walking frequency: ' num2str(settings{1}(2)) 'Hz, Duty factor: ' num2str(settings{1}(3))]})
box on
ax = gca;
ax.BoxStyle = 'full';
ax.LineWidth = 1.5;
ax.XDir='reverse';
ax.YDir='reverse';

hold on;

%% modern species

% LAR | spine | height    | deviations
modern = [ ...
    58.59, 43.97/2, 0.501, 			11.32,	6.16/2, 	0.021; ...	%caiman	 hind
]
    

logMAT = [];
devScl=1;
for k=1
    [x, y, z] = ellipsoid(modern(k,1) , modern(k,2), modern(k,3),           devScl*modern(k,4) , devScl*modern(k,5), devScl*modern(k,6),17);
%     x=reshape(x, numel(x),1);
%     y=reshape(y, numel(y),1);
%     z=reshape(z, numel(z),1);
    
    logMAT = [logMAT, x, y, z];
end


N=sqrt(numel(x));
CO=[];
CO(:,:,1) = zeros(N); % red
CO(:,:,2) = ones(N)*0.4; % green
CO(:,:,3) = zeros(N).*linspace(0,1,N); % blue


s=surf(x,y,z, CO);
s.EdgeColor = [0,0.3,0];







