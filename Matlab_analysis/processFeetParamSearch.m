
close
clear all
load FINAL_075_7744_dense
% load FINAL_05_7744_dense

% crit = {energyTV, energyTVAbs, energyTVZeroIg, energyTS, footTrajectoryReachability, 
%             stability, stabilityRate, absolutePrecision, maxTorquePerc', smoothness', stanceTorque};

% critStr = {'energyTV', 'energyTVAbs', 'energyTVZeroIg', 'energyTS', 'footTrajectoryReachability',
%           'stability', 'stabilityRate', 'absolutePrecision', 'maxTorquePerc', 'smoothness', 'stanceTorque'};

critIndx = 4;
critSubIndx=1;
% get unique feet param
feetParam = unique(par_grid(4:5,:)', 'rows')';


% intersect(par_grid(4:5,:)',feetParam(:,1)','rows')

for k=[3 7 8 12]
   cutValMax = prctile(crit{k}, 97);
   crit{k}(crit{k}>cutValMax)=cutValMax;
   
   cutValMin = prctile(crit{k}, 3);
   crit{k}(crit{k}<cutValMin)=cutValMin;
end

for k=1:size(feetParam,2)
    % get all gaits for a specific feet param
    indx = find(ismember(par_grid(4:5,:)',feetParam(:,k)','rows'));
    indxNan = find(isnan(crit{12}(indx)));
    indx(indxNan)=[];

    CRIT = [crit{3}(indx) crit{7}(indx) crit{8}(indx) (crit{12}(indx)+1)/2];

    critFeet(k) = geomean(CRIT);
end
critFeet=critFeet/max(critFeet);
feetParam(1, critFeet == max(max(critFeet)))
feetParam(2, critFeet == max(max(critFeet)))
surf(reshape(feetParam(1,:),11,11)*100, reshape(feetParam(2,:),11,11), reshape(critFeet,11,11));

% colormap jet
xlabel('offset')
ylabel('stiffness')

